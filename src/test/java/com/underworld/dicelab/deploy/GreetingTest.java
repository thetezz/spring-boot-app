package com.underworld.dicelab.deploy;

import com.underworld.dicelab.model.Greeting;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GreetingTest {

    @Test
    public void greetingTest() {
        int id = 1;
        String testString = "test";
        Greeting greeting = new Greeting(1, testString);
        assertTrue(id == greeting.getId(), "Greeting ID should be 1");
        assertTrue(testString.equals(greeting.getContent()), "Content should be testString");
    }

}
