package com.underworld.dicelab.service;

import com.underworld.dicelab.model.Greeting;

/**
 * A Spring Bean service that provides a hard-coded greeting, that is autowired into the GreetingController
 * Note that the @Bean declaration is not here - it is declared in the Application class.
 */
public class GreetingService {

    private long counter;

    public GreetingService() {
        this.counter = 0;
    }

    public Greeting getGreeting() {
        this.counter++;
        return new Greeting(this.counter, "Hello, Dicelab!");
    }


}
