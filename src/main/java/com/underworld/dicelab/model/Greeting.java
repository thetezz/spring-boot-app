package com.underworld.dicelab.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * A simple greeting model object.  Below you can see some @Api annotations in action.
 * These are swagger annotations that allow the swagger plugin to general notes on the model
 * Note that these are much more customizable than is apparent here - this example
 * Serves solely to show the basic structure.
 */
@ApiModel
public class Greeting {

    @ApiModelProperty
    public long id;

    @ApiModelProperty
    public String content;

    public Greeting() {
    }

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;

    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}