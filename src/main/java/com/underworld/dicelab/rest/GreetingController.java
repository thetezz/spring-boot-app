package com.underworld.dicelab.rest;

import com.underworld.dicelab.model.Greeting;
import com.underworld.dicelab.service.GreetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * This controller serves a single GET call.
 * @RestController declares this class to be a RestController.
 * @RequestMapping provides a path to reach this controller.  It is a generic, top level path that is expanded by
 * further annotations.
 * @Api This is a swagger annotation that describes this API.
 */
@RestController
@RequestMapping("/api/v1")
@Api(value="/api/v1")
public class GreetingController {

    /**
     * @Autowired lets Spring know to inject this dependency on start up.  It is combined with the @Bean annotaiton in
     * the Application class that loads the application.
     */
    @Autowired
    GreetingService greetingService;

    Logger logger = LoggerFactory.getLogger(Application.class);

    GreetingController() {
        logger.info("GreetingController initialized");
    }

    private static final String template = "Hello, Dicelab!";

    /**
     * The @GetMapping annotation instructs this method to be called when the /api/v1/greeting endpoint is contacted
     * with a GET request.  @ResponseBody packs the Greeting into a json object.
     * @ApiOperation inserts this call and its data into the swagger.json.  Note there are a lot more options available,
     * but they are ommited as an effective summary of the structure
     * @return Greeting
     */
    @GetMapping(path = "/greeting")
    @ApiOperation(value = "Greeting", notes = "Get a greeting")
    public @ResponseBody Greeting getGreeting() {
        logger.info("Request for greeting received.");
        return greetingService.getGreeting();
    }


}