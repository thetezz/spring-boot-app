package com.underworld.dicelab.rest;

import com.underworld.dicelab.service.GreetingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.*;

/**
 * This is the application configuration file.  Note that it has two primary annotations
 * @SpringBootAplicaiton is a shorthand annotation that combines @Configuration with some other spring annotations,
 * essentially declaring this class as the main Spring app.
 * @Bean - this allows the @Autowired annotation in the GreetingController to inject this dependency directly.
 * This is a necessary step for each service that is introduce as an autowired dependency.
 */

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		logger.info("Application starting up");
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public GreetingService greetingService() {
		return new GreetingService();
	}


}
