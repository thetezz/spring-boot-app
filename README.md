# spring-template

This is a template for creating REST services, and is designed to be used for ATPC as a basis for project, and as a demonstration of the pipeline deployment process

It is designed to run on Java 8 with Tomcat 8.  

Sending a GET request to this endpoint:
{environment}/rest-demo/api/v1/greeting

Will return this json object:
{"id":8,"content":"Hello, Dicelab!"}

It has some minimal features and examples, such as:

-Contains a single service injected with @Bean and @Autowire
-Contains a single rest endpoing (described above)
-Has some simple notes on the annotations.
-Produces a swagger.json on build

NOTE:
The swagger annotation are very sparse at the moment - they become very 
cluttered, very quickly.  For more information on how to use those annotations,
refer to this documentation:  

http://docs.swagger.io/swagger-core/v1.5.0/apidocs/index.html.  